package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@GetMapping("/testPing")
	String testPing() {
		return new Date().toString();
	}


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}