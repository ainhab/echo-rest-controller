FROM adoptopenjdk:11-jre-hotspot

#WORKDIR /usr/src/app

#COPY . /usr/src/app
#RUN mvn package
COPY target/echo-rest-controller-*.jar /echo-rest-controller.jar
ENV PORT 8080
EXPOSE $PORT
#CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
ENTRYPOINT ["java", "-Dserver.port=${PORT}", "-jar", "/echo-rest-controller"]